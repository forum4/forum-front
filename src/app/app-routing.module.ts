import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "/topico-forum" },
  { path: "welcome", loadChildren: () => import("./pages/welcome/welcome.module").then( m => m.WelcomeModule ) },
  { path: "topico-forum", loadChildren: () => import("./pages/topico-forum/topico-forum.module").then( m => m.TopicoForumModule ) },
  { path: "duvidas", loadChildren: () => import("./pages/duvida/duvida.module").then( m => m.DuvidaModule ) },
  { path: "respostas", loadChildren: () => import("./pages/resposta/resposta.module").then( m => m.RespostaModule ) },
  { path: "tags", loadChildren: () => import("./pages/tag/tag.module").then( m => m.TagModule )},
  { path: "perfil", loadChildren: () => import("./pages/perfil/perfil.module").then( m => m.PerfilModule ) }
];

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
