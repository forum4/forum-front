import { registerLocaleData } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import pt from "@angular/common/locales/pt";
import { NgModule } from "@angular/core";
import { FormBuilder, FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzDrawerService } from "ng-zorro-antd/drawer";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NZ_I18N, pt_BR } from "ng-zorro-antd/i18n";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { NzMessageService } from "ng-zorro-antd/message";
import { environment } from "../environments/environment";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CORE_API_URL, TOKEN_STORAGE_KEY } from "./core/core.constants";
import { CoreModule } from "./core/core.module";
import { AutenticacaoService } from "./core/services/auth/autenticacao.service";
import { IconsProviderModule } from "./icons-provider.module";
import { LoginModule } from "./pages/login/login.module";
import { SharedModule } from "./shared/shared.module";

registerLocaleData( pt );

@NgModule( {
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    IconsProviderModule,
    NzLayoutModule,
    NzGridModule,
    NzAvatarModule,
    NzMenuModule,
    CoreModule,
    LoginModule,
    SharedModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: pt_BR },
    { provide: CORE_API_URL, useValue: environment.coreApiUrl },
    { provide: TOKEN_STORAGE_KEY, useValue: "topico-forum-front-token" },
    NzMessageService,
    FormBuilder,
    AutenticacaoService,
    NzDrawerService
  ],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
