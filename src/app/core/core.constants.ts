import {InjectionToken} from "@angular/core";

export const CORE_API_URL = new InjectionToken<string>("CORE_API_URL");
export const TOKEN_STORAGE_KEY = new InjectionToken<number>("TOKEN_STORAGE_KEY");
