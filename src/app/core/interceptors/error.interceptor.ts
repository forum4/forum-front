import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { NzMessageService } from "ng-zorro-antd/message";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private message: NzMessageService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle( request ).pipe(
      catchError(err => {
        if (err.status === 401) {
          this.message.error("Usuário ou senha incorretos");
          return throwError(err.error);
        } else if (err.status === 403) {
          this.message.error(err.error.message || "Acesso Negado");
          return throwError(err.error);
        } else if (err.error.message.includes("JWT expired")) {
          this.message.error("Acesso negadoo");
        }

        const error = err.error;

        if (error && error.hasOwnProperty("empty") && !error.empty) {
          if (error.errorPresent) {
            error.errorMessages.forEach(it => {
              this.message.error(it.mensagem);
            });
          }

          if (error.warningPresent) {
            error.warningMessages.forEach(it => {
              this.message.warning(it.mensagem);
            });
          }

          if (error.infoPresent) {
            error.infoMessages.forEach(it => {
              this.message.info(it.mensagem);
            });
          }
        } else {
          if ( error.message ) {
            const array = error.message.split( "|" );
            for ( const message of array ) {
              this.message.error( message );
            }
          } else {
            this.message.error( "Ocorreu um erro ao processar o comando. Tente novamente." );
          }
        }

        return throwError(error);
      })
    );
  }
}
