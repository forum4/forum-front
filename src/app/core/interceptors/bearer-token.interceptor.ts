import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { first, mergeMap } from "rxjs/operators";
import { AutenticacaoService } from "../services/auth/autenticacao.service";

@Injectable()
export class BearerTokenInterceptor implements HttpInterceptor {
  constructor(
    private autenticacaoService: AutenticacaoService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.autenticacaoService.authToken$.pipe(
      first(),
      mergeMap( it => {
        let authReq = request;

        if ( it ) {
          const headers = new HttpHeaders().set( "Authorization", `Bearer ${ it.token }` );
          authReq = request.clone( { headers } );
        }

        return next.handle( authReq );
      } )
    );
  }
}
