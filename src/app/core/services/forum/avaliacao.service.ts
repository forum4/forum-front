import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { TipoAvaliacao } from "../../models/enums/tipo-avaliacao.enum";

@Injectable()
export class AvaliacaoService {
  private url = "/core/v1/avaliacao";

  constructor(
    private http: HttpClient
  ) {
  }

  public avaliar(idResposta: number, tipoAvaliacao: TipoAvaliacao): Observable<number> {
    const params = new HttpParams()
      .set( "id_resposta", idResposta.toString() )
      .set( "tipo_avaliacao", tipoAvaliacao );
    return this.http.post<number>( this.url, {}, { params } );
  }
}
