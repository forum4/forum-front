import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PageResponse} from "../../models/comum/page-response.model";
import {Tag} from "../../models/forum/tag.model";

@Injectable()
export class TagService {
  private tagUrl = "/core/v1/tag";

  constructor(private http: HttpClient) {
  }

  public save(tag: Tag): Observable<number> {
    return this.http.post<number>( this.tagUrl, tag );
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>( `${ this.tagUrl }/${ id }` );
  }

  public getAll(search?: string, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<Tag>> {
    const params = new HttpParams()
      .set( "search", search ? search : "" )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );
    return this.http.get<PageResponse<Tag>>( this.tagUrl, { params } );
  }

  public getOne(id: number): Observable<Tag> {
    return this.http.get<Tag>( `${ this.tagUrl }/${ id }` );
  }
}
