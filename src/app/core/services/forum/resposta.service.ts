import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PageResponse } from "../../models/comum/page-response.model";
import { Resposta } from "../../models/forum/resposta.model";

@Injectable()
export class RespostaService {
  private respostaURL = "/core/v1/resposta";

  constructor(private http: HttpClient) {
  }

  public create(resposta: Resposta): Observable<number> {
    return this.http.post<number>( this.respostaURL, resposta );
  }

  public update(resposta: Resposta): Observable<number> {
    return this.http.put<number>( this.respostaURL, resposta );
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>( `${ this.respostaURL }/${ id }` );
  }

  public getAll(search?: number, idRelated?: number, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<Resposta>> {
    const params = new HttpParams()
      .set( "search", search ? search.toString() : "" )
      .set( "id_related", String( idRelated ? idRelated : null ) )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );
    return this.http.get<PageResponse<Resposta>>( this.respostaURL, { params } );
  }

  public getOne(id: number): Observable<Resposta> {
    return this.http.get<Resposta>( `${ this.respostaURL }/${ id }` );
  }
}
