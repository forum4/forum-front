import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PageResponse } from "../../models/comum/page-response.model";
import { Duvida } from "../../models/forum/duvida.model";

@Injectable()
export class DuvidaService {
  private duvidaUrl = "/core/v1/duvida";

  constructor(private http: HttpClient) {
  }

  public getOne(id: number): Observable<Duvida> {
    return this.http.get<Duvida>( `${ this.duvidaUrl }/${ id }` );
  }

  public getAll(search?: string, idRelated?: number, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<Duvida>> {
    const params = new HttpParams()
      .set( "search", search ? search : "" )
      .set( "id_related", String( idRelated ? idRelated : null ) )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );

    return this.http.get<PageResponse<Duvida>>( this.duvidaUrl, { params } );
  }

  public create(duvida: Duvida): Observable<number> {
    return this.http.post<number>( this.duvidaUrl, duvida );
  }

  public update(duvida: Duvida): Observable<number> {
    return this.http.put<number>( this.duvidaUrl, duvida );
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>( `${ this.duvidaUrl }/${ id }` );
  }

  public marcarComoRespondida(idDuvida: number, idResposta: number): Observable<void> {
    const params = new HttpParams().set( "id_resposta", idResposta.toString() );
    return this.http.post<void>( `${ this.duvidaUrl }/${ idDuvida }/marcar-como-respondida`, {}, { params } );
  }

}
