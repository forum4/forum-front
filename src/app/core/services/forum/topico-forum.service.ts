import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PageResponse } from "../../models/comum/page-response.model";
import { TopicoForum } from "../../models/forum/topico-forum.model";
import {Tipo} from "../../models/comum/tipo.model";

@Injectable()
export class TopicoForumService {
  private forumUrl = "/core/v1/topico-forum";

  constructor(private http: HttpClient) {
  }

  public save(forum: TopicoForum): Observable<number> {
    return this.http.post<number>( this.forumUrl, forum );
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>( `${ this.forumUrl }/${ id }` );
  }

  public getAll(search?: string, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<TopicoForum>> {
    const params = new HttpParams()
      .set( "search", search ? search : "" )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );
    return this.http.get<PageResponse<TopicoForum>>( this.forumUrl, { params } );
  }

  public getOne(id: number): Observable<TopicoForum> {
    return this.http.get<TopicoForum>( `${ this.forumUrl }/${ id }` );
  }

  public getByTag(listaTags: Array<number>): Observable<Array<TopicoForum>> {
    return this.http.post<Array<TopicoForum>>(`${ this.forumUrl }/tag`, listaTags);
  }
}
