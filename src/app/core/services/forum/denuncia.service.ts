import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {PageResponse} from "../../models/comum/page-response.model";
import {Denuncia} from "../../models/forum/denuncia.model";

@Injectable()
export class DenunciaService {
  private denunciaURL = "/core/v1/denuncia";

  constructor(private http: HttpClient) {
  }

  public create(denuncia: Denuncia): Observable<number> {
    return this.http.post<number>( this.denunciaURL, denuncia );
  }

  public update(denuncia: Denuncia): Observable<number> {
    return this.http.put<number>( this.denunciaURL, denuncia );
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>( `${ this.denunciaURL }/${ id }` );
  }

  public getAll(search?: number, idRelated?: number, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<Denuncia>> {
    const params = new HttpParams()
      .set( "search", search ? search.toString() : "" )
      .set( "id_related", String( idRelated ? idRelated : null ) )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );
    return this.http.get<PageResponse<Denuncia>>( this.denunciaURL, { params } );
  }

  public getOne(id: number): Observable<Denuncia> {
    return this.http.get<Denuncia>( `${ this.denunciaURL }/${ id }` );
  }

}
