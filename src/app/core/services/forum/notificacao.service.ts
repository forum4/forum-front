import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class NotificacaoService {
  private url = "/core/v1/notificacao";

  constructor(
    private http: HttpClient
  ) {
  }

  public salvarNotificacaoTopicoForum(idTopicoForum: number): Observable<number> {
    const params = new HttpParams().set( "id_topico_forum", idTopicoForum.toString() );

    return this.http.post<number>( this.url, {}, { params } );
  }

  public salvarNotificacaoDuvida(idDuvida: number): Observable<number> {
    const params = new HttpParams().set( "id_duvida", idDuvida.toString() );

    return this.http.post<number>( this.url, {}, { params } );
  }
}
