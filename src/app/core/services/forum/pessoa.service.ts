import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PageResponse } from "../../models/comum/page-response.model";
import { Pessoa } from "../../models/forum/pessoa.model";

@Injectable()
export class PessoaService {
  private pessoaUrl = "/core/v1/pessoa";

  constructor(
    private http: HttpClient
  ) {
  }

  public getAll(search?: string, currentPage: number = 1, pageSize: number = 20): Observable<PageResponse<Pessoa>> {
    const params = new HttpParams()
      .set( "search", search ? search : "" )
      .set( "page", currentPage.toString() )
      .set( "pageSize", pageSize.toString() );

    return this.http.get<PageResponse<Pessoa>>( this.pessoaUrl, { params } );
  }

  public getOne(id: number): Observable<Pessoa> {
    return this.http.get<Pessoa>( `${ this.pessoaUrl }/${ id }` );
  }

  public existsByUsername(id: number, username: string): Observable<boolean> {
    const params = new HttpParams()
      .set( "pessoa_id", id ? id.toString() : "" )
      .set( "username", username );
    return this.http.get<boolean>( `${ this.pessoaUrl }/existe-usuario`, { params } );
  }

  public existsByEmail(id: number, email: string): Observable<boolean> {
    const params = new HttpParams()
      .set( "pessoa_id", id ? id.toString() : "" )
      .set( "email", email );
    return this.http.get<boolean>( `${ this.pessoaUrl }/existe-email`, { params } );
  }

  public save(pessoa: Pessoa): Observable<number> {
    return this.http.post<number>( this.pessoaUrl, pessoa );
  }

  public delete(): Observable<void> {
    return this.http.delete<void>( this.pessoaUrl );
  }
}
