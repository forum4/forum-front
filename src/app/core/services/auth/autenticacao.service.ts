import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { TOKEN_STORAGE_KEY } from "../../core.constants";
import { AutenticacaoResponse } from "../../models/auth/autenticacao-response.model";
import { AuthToken } from "../../models/auth/auth.token.model";
import { User } from "../../models/auth/user.model";
import { TipoPapel } from "../../models/comum/tipo-papel.enum";
import {Pessoa} from "../../models/forum/pessoa.model";

@Injectable( { providedIn: "root" } )
export class AutenticacaoService {
  private authUrl = "core/v1/auth";
  private readonly currentUserSubject$: BehaviorSubject<User> = new BehaviorSubject<User>( null );
  private readonly authTokenSubject$: BehaviorSubject<AuthToken>;

  constructor(
    private http: HttpClient,
    @Inject( TOKEN_STORAGE_KEY ) private tokenStorageKey: string
  ) {
    this.authTokenSubject$ = new BehaviorSubject<AuthToken>( null );

    this.autoLogin();

    this.authTokenSubject$.subscribe( it => {
      if ( !!it ) {
        localStorage.setItem( this.tokenStorageKey, JSON.stringify( it ) );
      }
    } );
  }

  get isAdministrador(): boolean {
    return this.currentUserSubject$.value ? this.currentUserSubject$.value.papel === TipoPapel.ADMINISTRADOR : false;
  }

  get currentUser$(): Observable<User> {
    return this.currentUserSubject$.asObservable();
  }

  public atualizarUsuario(pessoa: User) {
    this.currentUserSubject$.next( pessoa );
    const stringToken = localStorage.getItem( this.tokenStorageKey ) || sessionStorage.getItem( this.tokenStorageKey );
    const storedToken = stringToken ? JSON.parse( stringToken ) : null;
    if ( !!storedToken ) {
      this.authTokenSubject$.next( storedToken );
    }
  }

  get authToken$(): Observable<AuthToken> {
    return this.authTokenSubject$.asObservable();
  }

  public login(login: string, password: string): Observable<AutenticacaoResponse> {
    const payload = {
      login,
      password
    };

    return this.http.post<AutenticacaoResponse>( `${ this.authUrl }/login`, {}, { params: { ...payload } } )
               .pipe( tap(
                 it => {
                   localStorage.setItem( "usuario", JSON.stringify( it.usuario ) );
                   localStorage.setItem( "papel", JSON.stringify( it.papel ) );
                   this.currentUserSubject$.next( it.usuario );
                   this.authTokenSubject$.next( { tipo: "forum-core", token: it.token } );
                 },
                 err => {
                   this.currentUserSubject$.next( null );
                   this.authTokenSubject$.next( null );
                   return err;
                 }
               ) );
  }

  public logout(): void {
    this.currentUserSubject$.next( null );
    this.authTokenSubject$.next( null );
    localStorage.removeItem( this.tokenStorageKey );
    localStorage.removeItem( "usuario" );
    localStorage.removeItem( "papel" );
  }

  private autoLogin(): void {
    const currentUser = localStorage.getItem( "usuario" );
    const storedUser = currentUser ? JSON.parse( currentUser ) : null;

    if ( !!storedUser ) {
      this.currentUserSubject$.next( storedUser );
    }

    const stringToken = localStorage.getItem( this.tokenStorageKey ) || sessionStorage.getItem( this.tokenStorageKey );
    const storedToken = stringToken ? JSON.parse( stringToken ) : null;
    if ( !!storedToken ) {
      this.authTokenSubject$.next( storedToken );
    }
  }
}
