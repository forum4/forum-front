export interface AuthToken {
  token: string;
  tipo: "mcore" | "forum-core";
}
