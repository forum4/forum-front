import { TipoPapel } from "../comum/tipo-papel.enum";
import { Tipo } from "../comum/tipo.model";

export interface User extends Tipo<number> {
  usuario: string;
  email: string;
  papel?: TipoPapel;
  dataNascimento?: Date;
}
