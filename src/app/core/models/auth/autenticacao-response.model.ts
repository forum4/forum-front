import { TipoPapel } from "../comum/tipo-papel.enum";
import { User } from "./user.model";

export interface AutenticacaoResponse {
  usuario: User;
  papel: TipoPapel;
  errorMessage: string;
  token: string;
}
