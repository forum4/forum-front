export interface PageResponse<T> {
  conteudo: Array<T>;
  paginaAtual: number;
  tamanhoPagina: number;
  totalElementos: number;
  totalPaginas: number;
}
