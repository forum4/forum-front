export interface QueryFilter {
  search?: string;
  page?: number;
  pageSize?: number;
  ids?: number[];
}
