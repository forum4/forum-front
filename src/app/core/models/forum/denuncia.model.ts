import {Tipo} from "../comum/tipo.model";
import {TipoDenunciaEnum} from "../comum/tipo-denuncia.enum";

export class Denuncia {
  id: number;
  descricao: string;
  pessoaCadastro?: Tipo<number>;
  dataCriado?: Date;
  idTipo: number;
  tipoDenuncia: TipoDenunciaEnum;
}
