import { Tipo } from "../comum/tipo.model";

export interface Pessoa extends Tipo<number> {
  usuario?: string;
  senha?: string;
  dataNascimento?: Date;
  email?: string;
}
