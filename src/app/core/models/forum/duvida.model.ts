import { Tipo } from "../comum/tipo.model";

export interface Duvida extends Tipo<number> {
  titulo?: string;
  descricao?: string;
  pessoaCadastro?: Tipo<number>;
  pessoaUltimaEdicao?: Tipo<number>;
  topicoForum?: Tipo<number>;
  respostaCorreta?: Tipo<number>;
  dataCriado?: Date;
  dataUltimaEdicao?: Date;
  tags?: Array<Tipo<number>>;
}
