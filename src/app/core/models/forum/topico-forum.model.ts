import { Tipo } from "../comum/tipo.model";

export interface TopicoForum extends Tipo<number> {
  descricao?: string;
  pessoaCadastro?: Tipo<number>;
  dataCriado?: Date;
  tags?: Array<Tipo<number>>;
}
