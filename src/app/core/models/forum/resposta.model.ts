import { Tipo } from "../comum/tipo.model";

export interface Resposta {
  id: number;
  resposta: string;
  likes?: number;
  dislikes?: number;
  duvida: Tipo<number>;
  pessoaCadastro?: Tipo<number>;
  pessoaUltimaEdicao?: Tipo<number>;
  dataCriado?: Date;
  dataUltimaEdicao?: Date
}
