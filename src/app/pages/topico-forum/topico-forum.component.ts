import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd/message";
import { NzModalService } from "ng-zorro-antd/modal";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { User } from "../../core/models/auth/user.model";
import { TipoDenunciaEnum } from "../../core/models/comum/tipo-denuncia.enum";
import { TipoPapel } from "../../core/models/comum/tipo-papel.enum";
import { TopicoForum } from "../../core/models/forum/topico-forum.model";
import { AutenticacaoService } from "../../core/services/auth/autenticacao.service";
import { NotificacaoService } from "../../core/services/forum/notificacao.service";
import { TopicoForumService } from "../../core/services/forum/topico-forum.service";
import { DenunciaFormComponent } from "../../shared/components/denuncia-form/denuncia-form.component";
import { TopicoForumFormComponent } from "./topico-forum-form/topico-forum-form.component";
import {TagService} from "../../core/services/forum/tag.service";
import {Tipo} from "../../core/models/comum/tipo.model";

@Component( {
  selector: "app-topico-forum",
  templateUrl: "topico-forum.component.html",
  styleUrls: [ "topico-forum.component.less" ]
} )
export class TopicoForumComponent implements OnInit {
  conteudo: Array<TopicoForum>;
  listaTags: Array<Tipo<number>> = [];
  selectedTags: Array<number> = [];
  paginaAtual = 1;
  tamanhoPagina = 20;
  totalElementos = 0;
  totalPaginas = 0;

  // Booleans
  carregando = false;

  constructor(
    private autenticacaoService: AutenticacaoService,
    private topicoForumService: TopicoForumService,
    private tagService: TagService,
    private modalService: NzModalService,
    private message: NzMessageService,
    private notificacaoService: NotificacaoService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.buscarTopicosForum();

    this.tagService.getAll(null, 1, 50).pipe(tap((response) => {
      this.listaTags = response.conteudo.map(it => {
        return {
          id: it.id,
          nome: it.descricao
        }
      });
    })).subscribe();

  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  mudarInfoTabela(event: number, resetPagina: boolean = false): void {
    if (resetPagina) {
      this.tamanhoPagina = event;
    } else {
      this.paginaAtual = event;
    }
    this.buscarTopicosForum(resetPagina);
  }

  deletar(topico: TopicoForum): void {
    this.topicoForumService.delete(topico.id).pipe(tap(_ => {
      this.message.success("Tópico excluído");
    }, _ => {
      this.message.error("Não foi possível excluir o tópico de fórum");
    }, () => {
      this.buscarTopicosForum(true);
    })).subscribe();
  }

  abrirModalTopicoForum(topico?: TopicoForum): void {
    const modal = this.modalService.create({
      nzTitle: "Tópico de Fórum",
      nzContent: TopicoForumFormComponent,
      nzComponentParams: {
        topicoForum: topico,
        listaTags: this.listaTags
      }
    });

    modal.afterClose.subscribe(_ => {
      this.buscarTopicosForum(true);
    });
  }

  abrirModalDenuncia(topico?: TopicoForum): void {
    const modal = this.modalService.create( {
      nzTitle: "Você realmente deseja denunciar esse tópico?",
      nzContent: DenunciaFormComponent,
      nzComponentParams: {
        tipoDenuncia: TipoDenunciaEnum.TOPICO_FORUM,
        idItem: topico.id
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarTopicosForum();
    } );
  }

  habilitarNotificacoes(topicoForum: TopicoForum): void {
    this.notificacaoService.salvarNotificacaoTopicoForum( topicoForum.id ).pipe( tap( _ => {
      this.message.success( `Notificações habilitadas para o tópico: ${ topicoForum.nome }` );
      this.buscarTopicosForum();
    } ) ).subscribe();
  }

  podeAlterar(topico: TopicoForum, usuario: User): boolean {
    return !!usuario && (usuario.id === topico.pessoaCadastro.id || usuario.papel === TipoPapel.ADMINISTRADOR);
  }

  abrirDuvidas(topico: TopicoForum): void {
    this.router.navigate( [ "/duvidas" ], { queryParams: { id_topico: topico.id } } );
  }

  handleChange(checked: boolean, tag: number): void {
    if (checked) {
      this.selectedTags.push(tag);
    } else {
      this.selectedTags = this.selectedTags.filter(t => t !== tag);
    }


    if (this.selectedTags.length > 0) {
      this.topicoForumService.getByTag(this.selectedTags).pipe(tap((response) => {
        this.conteudo = [...response];
      })).subscribe();
    } else {
      this.buscarTopicosForum();
    }

  }

  private buscarTopicosForum(resetPagina: boolean = false): void {
    this.carregando = true;
    if (resetPagina) {
      this.paginaAtual = 1;
    }

    this.topicoForumService.getAll(null, this.paginaAtual, this.tamanhoPagina).pipe(tap((response) => {
      this.conteudo = response.conteudo;
      this.paginaAtual = response.paginaAtual;
      this.tamanhoPagina = response.tamanhoPagina;
      this.totalElementos = response.totalElementos;
      this.totalPaginas = response.totalPaginas;
      this.carregando = false;
    }, _ => (this.carregando = false))).subscribe();
  }
}
