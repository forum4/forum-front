import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzModalModule, NzModalService } from "ng-zorro-antd/modal";
import { NzPopconfirmModule } from "ng-zorro-antd/popconfirm";
import { NzSpinModule } from "ng-zorro-antd/spin";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { NotificacaoService } from "../../core/services/forum/notificacao.service";
import { TopicoForumService } from "../../core/services/forum/topico-forum.service";
import { TopicoForumFormComponent } from "./topico-forum-form/topico-forum-form.component";
import { TopicoForumComponent } from "./topico-forum.component";
import {NzSelectModule} from "ng-zorro-antd/select";
import {TagService} from "../../core/services/forum/tag.service";
import {NzTagModule} from "ng-zorro-antd/tag";

const routes: Routes = [
  {
    path: "",
    component: TopicoForumComponent,
    children: []
  }
];

@NgModule( {
  declarations: [
    TopicoForumComponent,
    TopicoForumFormComponent
  ],
  imports: [
    CommonModule,
    NzTableModule,
    RouterModule.forChild(routes),
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzSpinModule,
    NzModalModule,
    NzToolTipModule,
    NzSelectModule,
    NzTagModule,
    FormsModule
  ],
  providers: [
    TopicoForumService,
    TagService,
    NzModalService,
    NotificacaoService
  ],
  entryComponents: [
    TopicoForumFormComponent
  ]
} )
export class TopicoForumModule {
}
