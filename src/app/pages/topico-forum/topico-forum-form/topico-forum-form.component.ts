import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalRef} from "ng-zorro-antd/modal";
import {tap} from "rxjs/operators";
import {TopicoForum} from "../../../core/models/forum/topico-forum.model";
import {TopicoForumService} from "../../../core/services/forum/topico-forum.service";
import {Tipo} from "../../../core/models/comum/tipo.model";

@Component( {
  selector: "app-topico-forum-form",
  templateUrl: "topico-forum-form.component.html",
  styleUrls: [ "topico-forum-form.component.less" ]
} )
export class TopicoForumFormComponent implements OnInit {
  @Input() topicoForum: TopicoForum;
  @Input() listaTags: Array<Tipo<number>>;
  formulario: FormGroup;
  salvando = false;

  tagsSelecionadas: Array<number> = [];

  constructor(
    private topicoForumService: TopicoForumService,
    private message: NzMessageService,
    private formBuilder: FormBuilder,
    private modalRef: NzModalRef
  ) {
    this.initializeForm();
  }

  ngOnInit(): void {
    if ( !!this.topicoForum ) {
      this.formulario.patchValue( { ...this.topicoForum } );
    }

    if (this.topicoForum.tags !== null && this.topicoForum.tags.length > 0) {
      this.tagsSelecionadas = this.topicoForum.tags.map(it => it.id);
    }

  }

  initializeForm(): void {
    this.formulario = this.formBuilder.group( {
      id: [ null ],
      nome: [ null, Validators.required ],
      descricao: [ null, Validators.required ],
      tags: [[], null]
    } );
  }

  handleSalvar(): void {
    if ( this.formulario.invalid ) {
      Object.values( this.formulario.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return;
    }

    this.salvando = true;
    const topicoForum : TopicoForum = this.formulario.getRawValue();
    topicoForum.tags = this.tagsSelecionadas.map(it => {
      return this.listaTags.find(item => item.id === it);
    });

    this.topicoForumService.save( topicoForum ).pipe( tap( (next) => {
        this.message.success( "Tópico salvo" );
        this.handleFechar( next );
        this.salvando = false;
      },
      _ => {
        this.message.error( "Não foi possível salvar" );
        this.salvando = false;
      } ) ).subscribe();
  }

  handleFechar(id?: number): void {
    this.modalRef.close( { id } );
  }
}
