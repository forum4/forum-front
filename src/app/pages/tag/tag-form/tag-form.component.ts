import {Component, Input, OnInit} from '@angular/core';
import {Tag} from "../../../core/models/forum/tag.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalRef} from "ng-zorro-antd/modal";
import {TagService} from "../../../core/services/forum/tag.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-tag-form',
  templateUrl: './tag-form.component.html',
  styleUrls: ['./tag-form.component.less']
})
export class TagFormComponent implements OnInit {
  @Input() tag: Tag;
  formulario: FormGroup;
  salvando = false;

  constructor(
    private message: NzMessageService,
    private formBuilder: FormBuilder,
    private modalRef: NzModalRef,
    private tagService: TagService
  ) {
    this.initializeForm();
  }

  ngOnInit(): void {
    if ( !!this.tag ) {
      this.formulario.patchValue( { ...this.tag } );
    }
  }

  initializeForm(): void {
    this.formulario = this.formBuilder.group( {
      id: [ null ],
      descricao: [ null, Validators.required ]
    } );
  }

  handleSalvar(): void {
    if ( this.formulario.invalid ) {
      Object.values( this.formulario.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return;
    }

    this.salvando = true;
    const tag = this.formulario.getRawValue();

    this.tagService.save( tag ).pipe( tap( (next) => {
        this.message.success( "Tag salva" );
        this.handleFechar( next );
        this.salvando = false;
      },
      _ => {
        this.message.error( "Não foi possível salvar" );
        this.salvando = false;
      } ) ).subscribe();
  }

  handleFechar(id?: number): void {
    this.modalRef.close( { id } );
  }
}
