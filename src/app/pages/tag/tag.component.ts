import {Component, OnInit} from '@angular/core';
import {User} from "../../core/models/auth/user.model";
import {TipoPapel} from "../../core/models/comum/tipo-papel.enum";
import {Tag} from "../../core/models/forum/tag.model";
import {AutenticacaoService} from "../../core/services/auth/autenticacao.service";
import {NzModalService} from "ng-zorro-antd/modal";
import {Router} from "@angular/router";
import {TagService} from "../../core/services/forum/tag.service";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {NzMessageService} from "ng-zorro-antd/message";
import {TagFormComponent} from "./tag-form/tag-form.component";

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.less']
})
export class TagComponent implements OnInit {
  conteudo: Array<Tag>;
  paginaAtual = 1;
  tamanhoPagina = 20;
  totalElementos = 0;
  totalPaginas = 0;

  // Booleans
  carregando = false;
  constructor(
    private autenticacaoService: AutenticacaoService,
    private modalService: NzModalService,
    private message: NzMessageService,
    private router: Router,
    private tagService: TagService
  ) { }

  ngOnInit(): void {
    this.buscarTags();
  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  mudarInfoTabela(event: number, resetPagina: boolean = false): void {
    if (resetPagina) {
      this.tamanhoPagina = event;
    } else {
      this.paginaAtual = event;
    }
    this.buscarTags(resetPagina);
  }

  deletar(tag: Tag): void {
    this.tagService.delete(tag.id).pipe(tap(_ => {
      this.message.success("Tag excluída");
    }, _ => {
      this.message.error("Não foi possível excluir a tag");
    }, () => {
      this.buscarTags(true);
    })).subscribe();
  }

  abrirModalTag(tag?: Tag): void {
    const modal = this.modalService.create({
      nzTitle: "Tag",
      nzContent: TagFormComponent,
      nzComponentParams: {
        tag: tag
      }
    });

    modal.afterClose.subscribe(_ => {
      this.buscarTags(true);
    });
  }

  podeVisualizar(usuario: User): boolean {
    return !!usuario && usuario.papel === TipoPapel.ADMINISTRADOR;
  }

  private buscarTags(resetPagina: boolean = false): void {
    this.carregando = true;
    if (resetPagina) {
      this.paginaAtual = 1;
    }

    this.tagService.getAll(null, this.paginaAtual, this.tamanhoPagina).pipe(tap((response) => {
      this.conteudo = response.conteudo;
      this.paginaAtual = response.paginaAtual;
      this.tamanhoPagina = response.tamanhoPagina;
      this.totalElementos = response.totalElementos;
      this.totalPaginas = response.totalPaginas;
      this.carregando = false;
    }, _ => (this.carregando = false))).subscribe();
  }
}
