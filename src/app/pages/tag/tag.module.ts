import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagComponent } from './tag.component';
import {RouterModule, Routes} from "@angular/router";
import {TopicoForumComponent} from "../topico-forum/topico-forum.component";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzModalModule, NzModalService} from "ng-zorro-antd/modal";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {TagService} from "../../core/services/forum/tag.service";
import { TagFormComponent } from './tag-form/tag-form.component';
const routes: Routes = [
  {
    path: "",
    component: TagComponent,
    children: []
  }
];
@NgModule({
  declarations: [TagComponent, TagFormComponent],
  imports: [
    CommonModule,
    NzTableModule,
    RouterModule.forChild( routes ),
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzSpinModule,
    NzModalModule,
    NzToolTipModule
  ],
  providers: [
    NzModalService,
    TagService
  ]
})
export class TagModule { }
