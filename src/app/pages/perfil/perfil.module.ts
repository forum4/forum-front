import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PerfilComponent} from './perfil.component';
import {RouterModule, Routes} from "@angular/router";
import {PessoaService} from "../../core/services/forum/pessoa.service";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {SharedModule} from "../../shared/shared.module";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {AutenticacaoService} from "../../core/services/auth/autenticacao.service";
import {NzAvatarModule} from "ng-zorro-antd/avatar";

const routes: Routes = [
  {
    path: "",
    component: PerfilComponent,
    children: []
  }
];


@NgModule({
  declarations: [PerfilComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild( routes ),
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzSpinModule,
    NzToolTipModule,
    NzDividerModule,
    NzDatePickerModule,
    NzAvatarModule
  ],
  providers: [
    PessoaService,
    AutenticacaoService
  ]
})
export class PerfilModule { }
