import {AfterContentInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {PessoaService} from "../../core/services/forum/pessoa.service";
import {Pessoa} from "../../core/models/forum/pessoa.model";
import {tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {User} from "../../core/models/auth/user.model";
import {AutenticacaoService} from "../../core/services/auth/autenticacao.service";

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.less']
})
export class PerfilComponent implements OnInit, AfterContentInit {
  formulario: FormGroup;
  pessoaEdicao: Pessoa;
  usuarioSessao: Pessoa;
  // Boolean
  salvando = false;

  constructor(
    private fb: FormBuilder,
    private messageService: NzMessageService,
    private pessoaService: PessoaService,
    private autenticacaoService: AutenticacaoService
  ) {

  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  ngOnInit(): void {
    this.user$.pipe(tap((usuario) => {
      this.usuarioSessao = usuario;
    })).subscribe();

    this.pessoaService.getOne(this.usuarioSessao.id).pipe(tap((pessoa) => {
      this.pessoaEdicao = pessoa;
    })).subscribe();

  }

  ngAfterContentInit() {
    this.initializeForm();
  }


  handleSalvar(): void {
    if ( this.formulario.invalid ) {
      Object.values( this.formulario.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return;
    }

    const pessoa: Pessoa = this.formulario.getRawValue();
    this.salvando = true;
    this.pessoaService.save( pessoa ).pipe( tap( id => {
      this.autenticacaoService.atualizarUsuario({
        usuario: pessoa.usuario,
        email: pessoa.email,
        dataNascimento: pessoa.dataNascimento
      } as User);
      this.messageService.success( "Usuário salvo" );
      this.salvando = false;

    }, _ => {
      this.messageService.error( "Não foi possível salvar" );
      this.salvando = false;
    } ) ).subscribe();
  }

  private initializeForm(): void {
    this.formulario = this.fb.group( {
      id: this.usuarioSessao?.id,
      nome: [ this.usuarioSessao?.nome, Validators.required ],
      usuario: [ this.usuarioSessao?.usuario, [ Validators.required ] ],
      dataNascimento: [ this.usuarioSessao?.dataNascimento, Validators.required ],
      email: [ this.usuarioSessao?.email, [ Validators.required, Validators.email ] ]
    } );
  }
}
