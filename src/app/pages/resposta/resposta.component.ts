import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { formatDistance } from "date-fns";
import { NzMessageService } from "ng-zorro-antd/message";
import { NzModalService } from "ng-zorro-antd/modal";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { User } from "../../core/models/auth/user.model";
import { TipoPapel } from "../../core/models/comum/tipo-papel.enum";
import { TipoAvaliacao } from "../../core/models/enums/tipo-avaliacao.enum";
import { Duvida } from "../../core/models/forum/duvida.model";
import { Resposta } from "../../core/models/forum/resposta.model";
import { AutenticacaoService } from "../../core/services/auth/autenticacao.service";
import { AvaliacaoService } from "../../core/services/forum/avaliacao.service";
import { DuvidaService } from "../../core/services/forum/duvida.service";
import { RespostaService } from "../../core/services/forum/resposta.service";
import { RespostaFormComponent } from "./resposta-form/resposta-form.component";
import {DenunciaFormComponent} from "../../shared/components/denuncia-form/denuncia-form.component";
import {TipoDenunciaEnum} from "../../core/models/comum/tipo-denuncia.enum";

@Component( {
  selector: "app-resposta",
  templateUrl: "./resposta.component.html",
  styleUrls: [ "./resposta.component.less" ]
} )
export class RespostaComponent implements OnInit {
  conteudo: Array<Resposta>;
  duvida: Duvida;

  // Conteúdo da página
  paginaAtual = 1;
  tamanhoPagina = 20;
  totalElementos = 0;
  totalPaginas = 0;

  respostas: Resposta[] = [];
  // Booleanos
  carregando = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private respostaService: RespostaService,
    private duvidaService: DuvidaService,
    private modalService: NzModalService,
    private message: NzMessageService,
    private avaliacaoService: AvaliacaoService,
    private autenticacaoService: AutenticacaoService) {
  }

  ngOnInit(): void {
    const idDuvida = Number( this.route.snapshot.queryParamMap.get( "id_duvida" ) );
    this.buscarDuvida( idDuvida );
  }

  voltar(): void {
    this.router.navigate( [ "/duvidas" ], { queryParams: { id_topico: this.duvida?.topicoForum?.id } } );
  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  buscarDuvida(idDuvida: number): void {
    this.duvidaService.getOne( idDuvida ).subscribe( response => {
      this.duvida = response;
      this.buscarRespostas( true );
    } );
  }

  mudarInfoTabela(event: number, resetPagina: boolean = false): void {
    if ( resetPagina ) {
      this.tamanhoPagina = event;
    } else {
      this.paginaAtual = event;
    }
    this.buscarRespostas( resetPagina );
  }

  buscarRespostas(resetPagina: boolean = false): void {
    this.carregando = true;
    if ( resetPagina ) {
      this.paginaAtual = 1;
    }

    this.respostaService.getAll( null, this.duvida.id, this.paginaAtual, this.tamanhoPagina ).pipe( tap( response => {
      this.conteudo = response.conteudo;
      this.paginaAtual = response.paginaAtual;
      this.tamanhoPagina = response.tamanhoPagina;
      this.totalElementos = response.totalElementos;
      this.totalPaginas = response.totalPaginas;
      this.carregando = false;
    }, _ => {
      this.carregando = false;
      this.message.error( "Não foi possível buscar as dúvidas" );
    } ) ).subscribe();
  }

  deletar(resposta): void {
    this.respostaService.delete( resposta.id ).pipe( tap( _ => {
      this.message.success( "Resposta excluída com sucesso!" );
    }, _ => {
      this.message.error( "Não foi possível excluir a resposta." );
    }, () => {
      this.buscarRespostas();
    } ) ).subscribe();
  }

  abrirModalCadastrar(duvida: Duvida): void {
    const modal = this.modalService.create( {
      nzTitle: "Cadastro Resposta",
      nzContent: RespostaFormComponent,
      nzComponentParams: {
        acao: "cadastrar",
        duvida
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarRespostas();
    } );
  }

  abrirModalEditar(resposta: Resposta): void {
    const modal = this.modalService.create( {
      nzTitle: "Edição da Resposta",
      nzContent: RespostaFormComponent,
      nzComponentParams: {
        resposta,
        acao: "editar",
        duvida: this.duvida
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarRespostas();
    } );
  }

  abrirModalDenuncia(resposta: Resposta): void {
    const modal = this.modalService.create( {
      nzTitle: "Você realmente deseja denunciar essa resposta?",
      nzContent: DenunciaFormComponent,
      nzComponentParams: {
        tipoDenuncia: TipoDenunciaEnum.RESPOSTA,
        idItem: resposta.id
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarRespostas();
    } );
  }

  podeAlterar(resposta: Resposta, usuario: User): boolean {
    return usuario && (usuario.id === resposta.pessoaCadastro.id || usuario.papel === TipoPapel.ADMINISTRADOR);
  }

  podeMarcarComoRespondida(usuario: User): boolean {
    return usuario && usuario.id === this.duvida.pessoaCadastro.id;
  }

  marcarComoRespondida(resposta: Resposta): void {
    this.duvidaService.marcarComoRespondida( this.duvida.id, resposta.id ).pipe( tap( _ => {
      this.message.success( "Dúvida marcada como respondida" );
      this.buscarDuvida( this.duvida.id );
    }, _ => {
      this.message.error( "Não foi possível marcar dúvida como respondida" );
      this.buscarDuvida( this.duvida.id );
    } ) ).subscribe();
  }

  calcularIntervaloTempo(data: Date): string {
    return formatDistance( new Date( data ), new Date() );
  }

  like(idResposta: number): void {
    this.avaliacaoService.avaliar( idResposta, TipoAvaliacao.LIKE ).subscribe( _ => {
    }, _ => {
    }, () => {
      this.buscarRespostas();
    } );
  }

  dislike(idResposta: number): void {
    this.avaliacaoService.avaliar( idResposta, TipoAvaliacao.DISLIKE ).subscribe( _ => {
    }, _ => {
    }, () => {
      this.buscarRespostas();
    } );
  }

}
