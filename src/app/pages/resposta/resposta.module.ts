import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzCommentModule } from "ng-zorro-antd/comment";
import { NzEmptyModule } from "ng-zorro-antd/empty";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzListModule } from "ng-zorro-antd/list";
import { NzModalModule, NzModalService } from "ng-zorro-antd/modal";
import { NzPopconfirmModule } from "ng-zorro-antd/popconfirm";
import { NzSpinModule } from "ng-zorro-antd/spin";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzTimelineModule } from "ng-zorro-antd/timeline";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { AvaliacaoService } from "../../core/services/forum/avaliacao.service";
import { DuvidaService } from "../../core/services/forum/duvida.service";
import { RespostaService } from "../../core/services/forum/resposta.service";
import { TopicoForumService } from "../../core/services/forum/topico-forum.service";
import { SharedModule } from "../../shared/shared.module";
import { RespostaFormComponent } from "./resposta-form/resposta-form.component";
import { RespostaComponent } from "./resposta.component";

const routes: Routes = [
  {
    path: "",
    component: RespostaComponent,
    children: []
  }
];

@NgModule( {
  declarations: [ RespostaComponent, RespostaFormComponent ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild( routes ),
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzSpinModule,
    NzModalModule,
    NzToolTipModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzEmptyModule,
    NzTimelineModule,
    RouterModule,
    NzTableModule,
    NzCardModule

  ],
  providers: [
    RespostaService,
    DuvidaService,
    NzModalService,
    TopicoForumService,
    AvaliacaoService
  ],
  exports: [
    RespostaComponent
  ],
  entryComponents: [
    RespostaFormComponent
  ]
} )
export class RespostaModule {

}
