import {Component, Input, OnInit} from '@angular/core';
import {Duvida} from "../../../core/models/forum/duvida.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalRef} from "ng-zorro-antd/modal";
import {tap} from "rxjs/operators";
import {Resposta} from "../../../core/models/forum/resposta.model";
import {RespostaService} from "../../../core/services/forum/resposta.service";

@Component({
  selector: 'app-resposta-form',
  templateUrl: './resposta-form.component.html',
  styleUrls: ['./resposta-form.component.less']
})
export class RespostaFormComponent implements OnInit {

  @Input() resposta: Resposta;
  @Input() acao: String;
  @Input() duvida: Duvida;
  respostaForm: FormGroup;
  salvando = false;

  constructor(
    private respostaService: RespostaService,
    private message: NzMessageService,
    private formBuilder: FormBuilder,
    private modalRef: NzModalRef) {
  }

  ngOnInit(): void {
    this.initializeForm();
    if (this.resposta) {
      this.respostaForm.patchValue({ ...this.resposta });
    }
  }

  initializeForm(): void {
    this.respostaForm = this.formBuilder.group({
      id: [null],
      resposta: [null, Validators.required],
      duvida: [{ id: this.duvida.id, nome: this.duvida.nome }, Validators.required]
    });
  }

  validateForm(): boolean {
    if (this.respostaForm.invalid) {
      Object.values(this.respostaForm.controls).forEach(it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      });
      return false;
    }
    return true;
  }

  handleCadastrar(): void {
    if (!this.validateForm()) {
      return;
    }
    this.salvando = true;
    const resposta = this.respostaForm.getRawValue();

    this.respostaService.create(resposta).pipe(tap((next) => {
        this.message.success("Resposta cadastrada com sucesso!");
        this.handleFechar(next);
        this.salvando = false;
      },
      _ => {
        this.message.error("Não foi possível cadastrar a resposta.");
        this.salvando = false;
      })).subscribe();
  }

  handleEditar(): void {

    if (!this.validateForm()) {
      return;
    }

    this.salvando = true;
    const resposta = this.respostaForm.getRawValue();

    this.respostaService.update(resposta).pipe(tap((next) => {
        this.message.success("Dúvida alterada com sucesso!");
        this.handleFechar(next);
        this.salvando = false;
      },
      _ => {
        this.message.error("Não foi possível editar a dúvida.");
        this.salvando = false;
      })).subscribe();
  }

  handleFechar(id?: number): void {
    this.modalRef.close({ id });
  }

}
