import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzMessageService } from "ng-zorro-antd/message";
import { NzModalRef } from "ng-zorro-antd/modal";
import { tap } from "rxjs/operators";
import { Duvida } from "src/app/core/models/forum/duvida.model";
import { TopicoForum } from "src/app/core/models/forum/topico-forum.model";
import { DuvidaService } from "src/app/core/services/forum/duvida.service";
import {Tipo} from "../../../core/models/comum/tipo.model";

@Component( {
  selector: "app-duvida-form",
  templateUrl: "./duvida-form.component.html",
  styleUrls: [ "./duvida-form.component.less" ]
} )

export class DuvidaFormComponent implements OnInit {
  @Input() duvida: Duvida;
  @Input() topico: TopicoForum;
  @Input() listaTags: Array<Tipo<number>>;
  duvidaForm: FormGroup;
  salvando = false;
  tagsSelecionadas: Array<number> = [];

  constructor(
    private duvidaService: DuvidaService,
    private message: NzMessageService,
    private formBuilder: FormBuilder,
    private modalRef: NzModalRef) {
  }

  ngOnInit(): void {
    this.initializeForm();
    if ( this.duvida ) {
      this.duvidaForm.patchValue( { ...this.duvida } );
    }

    if (this.duvida.tags !== null && this.duvida.tags.length > 0) {
      this.tagsSelecionadas = this.duvida.tags.map(it => it.id);
    }
  }

  initializeForm(): void {
    this.duvidaForm = this.formBuilder.group( {
      id: [ null ],
      nome: [ null, Validators.required ],
      descricao: [ null, Validators.required ],
      topicoForum: [ { id: this.topico.id, nome: this.topico.nome }, Validators.required ],
      tags: [[], null]
    } );
  }

  validateForm(): boolean {
    if ( this.duvidaForm.invalid ) {
      Object.values( this.duvidaForm.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return false;
    }
    return true;
  }

  handleCadastrar(): void {
    if ( !this.validateForm() ) {
      return;
    }
    this.salvando = true;
    const duvida = this.duvidaForm.getRawValue();
    duvida.tags = this.tagsSelecionadas.map(it => {
      return this.listaTags.find(item => item.id === it);
    });

    this.duvidaService.create( duvida ).pipe( tap( (next) => {
        this.message.success( "Dúvida cadastrada com sucesso!" );
        this.handleFechar( next );
        this.salvando = false;
      },
      _ => {
        this.message.error( "Não foi possível cadastrar a dúvida." );
        this.salvando = false;
      } ) ).subscribe();
  }

  handleEditar(): void {

    if ( !this.validateForm() ) {
      return;
    }

    this.salvando = true;
    const duvida = this.duvidaForm.getRawValue();
    duvida.tags = this.tagsSelecionadas.map(it => {
      return this.listaTags.find(item => item.id === it);
    });

    this.duvidaService.update( duvida ).pipe( tap( (next) => {
        this.message.success( "Dúvida alterada com sucesso!" );
        this.handleFechar( next );
        this.salvando = false;
      },
      _ => {
        this.message.error( "Não foi possível editar a dúvida." );
        this.salvando = false;
      } ) ).subscribe();
  }

  handleFechar(id?: number): void {
    this.modalRef.close( { id } );
  }
}
