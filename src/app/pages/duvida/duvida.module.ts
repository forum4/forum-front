import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzCommentModule } from "ng-zorro-antd/comment";
import { NzEmptyModule } from "ng-zorro-antd/empty";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzListModule } from "ng-zorro-antd/list";
import { NzModalModule, NzModalService } from "ng-zorro-antd/modal";
import { NzPopconfirmModule } from "ng-zorro-antd/popconfirm";
import { NzSpinModule } from "ng-zorro-antd/spin";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { DuvidaService } from "../../core/services/forum/duvida.service";
import { NotificacaoService } from "../../core/services/forum/notificacao.service";
import { TopicoForumService } from "../../core/services/forum/topico-forum.service";
import { DuvidaComponent } from "../duvida/duvida.component";
import { DuvidaFormComponent } from "./duvida-form/duvida-form.component";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTagModule} from "ng-zorro-antd/tag";
import {TagService} from "../../core/services/forum/tag.service";

const routes: Routes = [
  {
    path: "",
    component: DuvidaComponent,
    children: []
  }
];

@NgModule( {
  declarations: [
    DuvidaComponent,
    DuvidaFormComponent,
  ],
  imports: [
    CommonModule,
    NzTableModule,
    RouterModule.forChild( routes ),
    NzButtonModule,
    NzIconModule,
    NzPopconfirmModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    NzSpinModule,
    NzModalModule,
    NzToolTipModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzEmptyModule,
    NzCardModule,
    NzSelectModule,
    NzTagModule,
    FormsModule
  ],
  providers: [
    DuvidaService,
    NzModalService,
    TopicoForumService,
    NotificacaoService,
    TagService
  ],
  entryComponents: [
    DuvidaFormComponent
  ]
} )
export class DuvidaModule {
}
