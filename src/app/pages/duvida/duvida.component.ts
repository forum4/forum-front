import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { formatDistance } from "date-fns";
import { NzMessageService } from "ng-zorro-antd/message";
import { NzModalService } from "ng-zorro-antd/modal";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { User } from "src/app/core/models/auth/user.model";
import { TipoPapel } from "src/app/core/models/comum/tipo-papel.enum";
import { Duvida } from "src/app/core/models/forum/duvida.model";
import { AutenticacaoService } from "src/app/core/services/auth/autenticacao.service";
import { TipoDenunciaEnum } from "../../core/models/comum/tipo-denuncia.enum";
import { TopicoForum } from "../../core/models/forum/topico-forum.model";
import { DuvidaService } from "../../core/services/forum/duvida.service";
import { NotificacaoService } from "../../core/services/forum/notificacao.service";
import { TopicoForumService } from "../../core/services/forum/topico-forum.service";
import { DenunciaFormComponent } from "../../shared/components/denuncia-form/denuncia-form.component";
import { DuvidaFormComponent } from "./duvida-form/duvida-form.component";
import {Tipo} from "../../core/models/comum/tipo.model";
import {TagService} from "../../core/services/forum/tag.service";

@Component( {
  selector: "app-duvida",
  templateUrl: "./duvida.component.html",
  styleUrls: [ "./duvida.component.less" ]
} )
export class DuvidaComponent implements OnInit {
  conteudo: Array<Duvida>;
  idTopicoForum: number;
  topico: TopicoForum;
  listaTags: Array<Tipo<number>> = [];
  selectedTags: Array<number> = [];
  // Conteúdo da página
  paginaAtual = 1;
  tamanhoPagina = 20;
  totalElementos = 0;
  totalPaginas = 0;

  // Booleanos
  carregando = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private duvidaService: DuvidaService,
    private topicoForumService: TopicoForumService,
    private tagService: TagService,
    private notificacaoService: NotificacaoService,
    private modalService: NzModalService,
    private message: NzMessageService,
    private autenticacaoService: AutenticacaoService) {
  }

  ngOnInit(): void {
    this.idTopicoForum = Number( this.route.snapshot.queryParamMap.get( "id_topico" ) );

    this.buscarTopicoForum();

    this.tagService.getAll(null, 1, 50).pipe(tap((response) => {
      this.listaTags = response.conteudo.map(it => {
        return {
          id: it.id,
          nome: it.descricao
        }
      });
    })).subscribe();
  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  mudarInfoTabela(event: number, resetPagina: boolean = false): void {
    if ( resetPagina ) {
      this.tamanhoPagina = event;
    } else {
      this.paginaAtual = event;
    }
    this.buscarDuvidas( resetPagina );
  }

  buscarDuvidas(resetPagina: boolean = false): void {
    this.carregando = true;
    if ( resetPagina ) {
      this.paginaAtual = 1;
    }

    this.duvidaService.getAll( null, this.idTopicoForum, this.paginaAtual, this.tamanhoPagina ).pipe( tap( response => {
      this.conteudo = response.conteudo;
      this.paginaAtual = response.paginaAtual;
      this.tamanhoPagina = response.tamanhoPagina;
      this.totalElementos = response.totalElementos;
      this.totalPaginas = response.totalPaginas;
      this.carregando = false;
    }, _ => {
      this.carregando = false;
      this.message.error( "Não foi possível buscar as dúvidas" );
    } ) ).subscribe();
  }

  buscarTopicoForum(): void {
    this.topicoForumService.getOne( this.idTopicoForum ).subscribe( response => {
      this.topico = response;
      this.buscarDuvidas( true );
    } );
  }

  calcularIntervaloTempo(data: Date): string {
    return formatDistance( new Date( data ), new Date() );
  }

  deletar(duvida): void {
    this.duvidaService.delete( duvida.id ).pipe( tap( _ => {
      this.message.success( "Dúvida excluída com sucesso!" );
    }, _ => {
      this.message.error( "Não foi possível excluir a dúvida." );
    }, () => {
      this.buscarDuvidas();
    } ) ).subscribe();
  }

  abrirModalDuvida(duvida?: Duvida): void {
    const modal = this.modalService.create( {
      nzTitle: "Edição de Dúvida",
      nzContent: DuvidaFormComponent,
      nzComponentParams: {
        duvida,
        topico: this.topico,
        listaTags: this.listaTags
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarDuvidas( true );
    } );
  }

  abrirModalDenuncia(duvida: Duvida): void {
    const modal = this.modalService.create( {
      nzTitle: "Você realmente deseja denunciar essa dúvida?",
      nzContent: DenunciaFormComponent,
      nzComponentParams: {
        tipoDenuncia: TipoDenunciaEnum.DUVIDA,
        idItem: duvida.id
      }
    } );

    modal.afterClose.subscribe( _ => {
      this.buscarDuvidas();
    } );
  }

  habilitarNotificacoes(duvida: Duvida): void {
    this.notificacaoService.salvarNotificacaoDuvida( duvida.id ).pipe( tap( _ => {
      this.message.success( `Notificações habilitadas para a dúvida: ${ duvida.nome }` );
      this.buscarDuvidas();
    } ) ).subscribe();
  }

  abrirResposta(duvida: Duvida): void {
    this.router.navigate( [ "/respostas" ], { queryParams: { id_duvida: duvida.id } } );
  }

  podeAlterar(duvida: Duvida, usuario: User): boolean {
    return usuario && (usuario.id === duvida.pessoaCadastro.id || usuario.papel.toUpperCase() === TipoPapel.ADMINISTRADOR.toUpperCase());
  }

  handleChange(checked: boolean, tag: number): void {
    if (checked) {
      this.selectedTags.push(tag);
    } else {
      this.selectedTags = this.selectedTags.filter(t => t !== tag);
    }


    if (this.selectedTags.length > 0) {
      this.topicoForumService.getByTag(this.selectedTags).pipe(tap((response) => {
        this.conteudo = [...response];
      })).subscribe();
    } else {
      this.buscarDuvidas();
    }

  }
}

