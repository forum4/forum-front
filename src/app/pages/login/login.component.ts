import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzDrawerService } from "ng-zorro-antd/drawer";
import { Observable } from "rxjs";
import { User } from "../../core/models/auth/user.model";
import { AutenticacaoService } from "../../core/services/auth/autenticacao.service";
import { PessoaFormComponent } from "../../shared/components/pessoa-form.component";

@Component( {
  selector: "app-login",
  templateUrl: "login.component.html",
  styleUrls: [ "login.component.less" ]
} )
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private drawerService: NzDrawerService,
    private autenticacaoService: AutenticacaoService
  ) {
    this.initializeForm();
  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  ngOnInit(): void {
  }

  handleSubmit(): void {
    if ( this.loginForm.invalid ) {
      Object.values( this.loginForm.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
    }

    this.loading = true;
    const params = this.loginForm.getRawValue();

    this.autenticacaoService.login( params.login, params.password ).subscribe( it => {
      this.loading = false;
    }, _ => {
      this.loading = false;
    } );
  }

  handleLogout(): void {
    this.autenticacaoService.logout();
  }

  private initializeForm(): void {
    this.loginForm = this.formBuilder.group( {
      login: [ null, Validators.required ],
      password: [ null, Validators.required ]
    } );
  }

  public abrirDrawerPessoaCadastro(): void {
    const drawer = this.drawerService.create( {
      nzContent: PessoaFormComponent,
      nzWidth: "40%",
      nzTitle: "Usuário",
      nzFooter: null
    } );

    drawer.afterClose.subscribe();
  }
}
