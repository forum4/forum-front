import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {PessoaService} from "../core/services/forum/pessoa.service";
import {PessoaFormComponent} from "./components/pessoa-form.component";
import {LetrasIniciaisPipe} from "./pipes/letras-iniciais.pipe";
import {DenunciaFormComponent} from './components/denuncia-form/denuncia-form.component';
import {DenunciaService} from "../core/services/forum/denuncia.service";
import {NzModalModule} from "ng-zorro-antd/modal";

@NgModule( {
  declarations: [
    LetrasIniciaisPipe,
    PessoaFormComponent,
    DenunciaFormComponent
  ],
  exports: [
    LetrasIniciaisPipe,
    PessoaFormComponent
  ],
  imports: [
    CommonModule,
    NzFormModule,
    ReactiveFormsModule,
    NzInputModule,
    CommonModule,
    NzDatePickerModule,
    NzSpinModule,
    NzButtonModule,
    NzIconModule,
    NzDividerModule,
    NzModalModule
  ],
  providers: [
    PessoaService,
    DenunciaService
  ]
} )
export class SharedModule {
}
