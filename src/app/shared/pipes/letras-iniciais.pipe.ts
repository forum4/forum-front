import { Pipe, PipeTransform } from "@angular/core";

@Pipe( {
  name: "letrasIniciais"
} )
export class LetrasIniciaisPipe implements PipeTransform {
  transform(value: string, qtd?: number): string {
    if ( !value ) {
      return value;
    }

    const siglas: string[] = [];
    for ( const nom of value.split( " " ) ) {
      siglas.push( nom.charAt( 0 ).toUpperCase() );
    }

    if ( !qtd ) {
      qtd = 2;
    }

    if ( qtd > siglas.length ) {
      qtd = siglas.length;
    }

    return siglas.slice( 0, qtd ).join( "." );
  }
}
