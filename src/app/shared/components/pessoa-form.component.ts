import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzDrawerRef } from "ng-zorro-antd/drawer";
import { NzMessageService } from "ng-zorro-antd/message";
import { tap } from "rxjs/operators";
import { Pessoa } from "../../core/models/forum/pessoa.model";
import { PessoaService } from "../../core/services/forum/pessoa.service";

@Component( {
  selector: "app-pessoa-form",
  templateUrl: "pessoa-form.component.html",
  styleUrls: [ "pessoa-form.component.less" ]
} )
export class PessoaFormComponent implements OnInit {
  formulario: FormGroup;

  // Boolean
  salvando = false;

  constructor(
    private fb: FormBuilder,
    private messageService: NzMessageService,
    private drawerRef: NzDrawerRef,
    private pessoaService: PessoaService
  ) {
    this.initializeForm();
  }

  ngOnInit(): void {
  }

  handleFechar(emit?: { id: number }): void {
    this.salvando = false;
    this.drawerRef.close( emit );
  }

  handleSalvar(): void {
    if ( this.formulario.invalid ) {
      Object.values( this.formulario.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return;
    }

    const pessoa: Pessoa = this.formulario.getRawValue();
    this.salvando = true;
    this.pessoaService.save( pessoa ).pipe( tap( id => {
      this.messageService.success( "Usuário salvo" );
      this.handleFechar( { id } );
    }, _ => {
      this.messageService.error( "Não foi possível salvar" );
      this.salvando = false;
    } ) ).subscribe();
  }

  private initializeForm(): void {
    this.formulario = this.fb.group( {
      id: null,
      nome: [ null, Validators.required ],
      usuario: [ null, [ Validators.required ] ],
      senha: [ null ],
      dataNascimento: [ new Date(), Validators.required ],
      email: [ null, [ Validators.required, Validators.email ] ]
    } );
  }
}
