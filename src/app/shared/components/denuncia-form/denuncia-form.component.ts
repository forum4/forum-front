import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TipoDenunciaEnum} from "../../../core/models/comum/tipo-denuncia.enum";
import {NzMessageService} from "ng-zorro-antd/message";
import {NzModalRef} from "ng-zorro-antd/modal";
import {DenunciaService} from "../../../core/services/forum/denuncia.service";
import {Denuncia} from "../../../core/models/forum/denuncia.model";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-denuncia-form',
  templateUrl: './denuncia-form.component.html',
  styleUrls: ['./denuncia-form.component.less']
})
export class DenunciaFormComponent implements OnInit {
  @Input() tipoDenuncia: TipoDenunciaEnum;
  @Input() idItem: number;

  denunciaForm: FormGroup;
  salvando = false;

  constructor(private denunciaService: DenunciaService,
              private message: NzMessageService,
              private formBuilder: FormBuilder,
              private modalRef: NzModalRef) {

  }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.denunciaForm = this.formBuilder.group( {
      id: [ null ],
      descricao: [ null, Validators.required ]
    } );
  }

  validateForm(): boolean {
    if ( this.denunciaForm.invalid ) {
      Object.values( this.denunciaForm.controls ).forEach( it => {
        it.markAsDirty();
        it.updateValueAndValidity();
      } );
      return false;
    }
    return true;
  }

  handleCadastrar(): void {
    if ( !this.validateForm() ) {
      return;
    }
    this.salvando = true;
    const denuncia: Denuncia = this.denunciaForm.getRawValue();
    denuncia.idTipo = this.idItem;
    denuncia.tipoDenuncia = this.tipoDenuncia;
    this.denunciaService.create( denuncia ).pipe( tap( (next) => {
        this.message.success( "Denúncia cadastrada com sucesso!" );
        this.handleFechar( next );
        this.salvando = false;
      },
      _ => {
        this.message.error( "Não foi possível cadastrar a denúncia." );
        this.salvando = false;
      } ) ).subscribe();
  }

  handleFechar(id?: number): void {
    this.modalRef.close( { id } );
  }
}
