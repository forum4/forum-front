import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "./core/models/auth/user.model";
import { AutenticacaoService } from "./core/services/auth/autenticacao.service";
import {TopicoForum} from "./core/models/forum/topico-forum.model";
import {TipoPapel} from "./core/models/comum/tipo-papel.enum";

@Component( {
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: [ "./app.component.less" ]
} )
export class AppComponent {
  isCollapsed = false;

  constructor(
    private autenticacaoService: AutenticacaoService
  ) {
  }

  get user$(): Observable<User> {
    return this.autenticacaoService.currentUser$;
  }

  logout(): void {
    this.autenticacaoService.logout();
  }

  podeVisualizar(usuario: User): boolean {
    return !!usuario && usuario.papel === TipoPapel.ADMINISTRADOR;
  }
}
